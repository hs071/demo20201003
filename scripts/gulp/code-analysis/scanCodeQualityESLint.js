/**
 * @name scanCodeQualityESLint.js
 */
import {
  gulp,
  path,
  parseStringPromise,
  readFileSync,
  writeFileSync
} from '../common/utils';
/**
 * @name scanCodeQualityESLint
 * @description scan Code Quality for ESLint
 */

export default async function scanCodeQualityESLint() {
  // 解析結果ファイルの絶対パスを取得
  const filePath_ = path.relative(__dirname, '.logs/eslint.xml');
  const filePath = path.join(__dirname, filePath_);
  const outFilePath_ = path.relative(__dirname, '.reports/eslint.html');
  const outFilePath = path.join(__dirname, outFilePath_);

  // パイプラインが読み取るファイル
  const RESULT_FILE = '.logs/eslint.violations.json';

  try {
    // XML -> JSON
    const xml = readFileSync(filePath);
    const result = await parseStringPromise(xml);
    const json = result.checkstyle.file;
    let violationLength = 0;
    if (json) {
      json.forEach((item) => {
        violationLength += item.error ? item.error.length : 0;
      });
    }

    if (violationLength === 0) {
      console.log('------ Code Static Analisys ------ ESLint Check OK');
    } else {
      console.log('------ Code Static Analisys ------ ESLint Check NG');
      console.log('------ ESLint Execution log ------');
      console.log('------ Output Directory ------');
      console.log(outFilePath);
    }

    // パイプラインが読み取りたい形式で保存
    const violations = {
      result: {
        violations: violationLength
      }
    };
    writeFileSync(RESULT_FILE, JSON.stringify(violations));
  } catch (err) {
    console.error(err);
  }
  return gulp.src('.');
}
