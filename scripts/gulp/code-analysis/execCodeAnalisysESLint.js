/**
 * @name execCodeAnalisysESLint.js
 */
import { gulp, shell, path, writeFileSync } from '../common/utils';

/**
 * @name execCodeAnalisysESLint
 * @description exec Code Analisys for ESLint
 */
export default async function execCodeAnalisysESLint() {
  try {
    // Command Create
    const commandCmd = 'yarn eslint:app';
    // target file
    const filePath_ = path.relative(__dirname, '**/lwc');
    // file path create
    const filePath = path.join(__dirname, filePath_);
    // result file
    const RESULT_FILE = '.logs/eslint.xml';
    const glob = require('glob');
    // log folder check
    glob(filePath, function (err, files) {
      if (err) {
        console.log(err);
      }
      console.log(files);
      if (files.length > 0) {
        console.log('ESLint Target files : ' + files.length);
        return gulp.src('.').pipe(
          shell(commandCmd, {
            verbose: true,
            ignoreErrors: false
          })
        );
      } else {
        console.log('warn:non-files - ESLint');
        writeFileSync(RESULT_FILE, createDummyFile());
      }
    });
  } catch (err) {
    console.error(err);
  }
  return gulp.src('.');
}
/**
 * @name createDummyFile
 * @description non-exec create dummy file
 */
function createDummyFile() {
  try {
    return '<?xml version="1.0" encoding="utf-8"?><checkstyle version="4.3"><file name="none"></file></checkstyle>';
  } catch {
    console.error(err);
  }
}
