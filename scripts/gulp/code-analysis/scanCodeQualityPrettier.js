/**
 * @name scanCodeQualityPrettier.js
 */
import { gulp, path, readFileSync } from '../common/utils';

/**
 * @name scanCodeQualityPrettier
 * @description scan Code Quality for Prettier
 */
export default async function scanCodeQualityPrettier() {
  // 解析結果ファイルの絶対パスを取得
  const filePath_ = path.relative(__dirname, '.logs/prettier-check.stderr.txt');
  const filePath = path.join(__dirname, filePath_);
  const outFilePath_ = path.relative(
    __dirname,
    '.logs/prettier-check.stderr.txt'
  );
  const outFilePath = path.join(__dirname, outFilePath_);
  try {
    // Error File Open
    const errorFile = readFileSync(filePath);

    if (errorFile.length === 0) {
      console.log('------ Code Format check ------ Prettier Check OK');
    } else {
      console.log('------ Code Format check ------ Prettier Check NG');
      console.log('------ Prettier Execution log ------');
      console.log('------ Output Directory ------');
      console.log(outFilePath);
    }
  } catch (err) {
    console.error(err);
  }
  return gulp.src('.');
}
