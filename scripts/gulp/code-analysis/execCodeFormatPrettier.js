/**
 * @name execCodeFormatPrettier.js
 */
import { gulp, shell } from '../common/utils';

/**
 * @name execCodeFormatPrettier
 * @description exec Code Analisys for Pmd
 */
export default async function execCodeFormatPrettier() {
  try {
    const commandCmd =
      'yarn prettier:check ' +
      '1> .logs/prettier-check.stdout.txt ' +
      '2> .logs/prettier-check.stderr.txt';

    return gulp.src('.').pipe(
      shell(commandCmd, {
        verbose: true,
        ignoreErrors: false
      })
    );
  } catch (err) {
    console.error(err);
  }
  return gulp.src('.');
}
