/**
 * @name execCodeAnalisysPmd.js
 */
import { gulp, shell, path, writeFileSync } from '../common/utils';

/**
 * @name execCodeAnalisysPmd
 * @description exec Code Analisys for Pmd
 */
export default async function execCodeAnalisysPmd() {
  try {
    // Command Create
    const commandCmd = 'yarn pmd:apex';
    // target file
    const filePath_ = path.relative(__dirname, '**/*.cls');
    // file path create
    const filePath = path.join(__dirname, filePath_);
    // result file
    const RESULT_FILE = '.logs/pmd.xml';
    const glob = require('glob');
    console.log('ApexPMD Dir Path : ' + filePath);
    glob(filePath, function (err, files) {
      if (files.length > 0) {
        console.log('ApexPMD Target files : ' + files.length);
        return gulp.src('.').pipe(
          shell(commandCmd, {
            verbose: true,
            ignoreErrors: false
          })
        );
      } else {
        console.log('warn:non-files - ApexPMD');
        writeFileSync(RESULT_FILE, createDummyFile());
      }
    });
  } catch (err) {
    console.error(err);
  }
  return gulp.src('.');
}

/**
 * @name createDummyFile
 * @description non-exec create dummy file
 */
function createDummyFile() {
  try {
    return '<?xml version="1.0" encoding="UTF-8"?><pmd xmlns="http://pmd.sourceforge.net/report/2.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http:///pmd.sourceforge.net/report/2.0.0 http://pmd.sourceforge.net/report_2_0_0.xsd" version="6.26.0" timestamp="none"></pmd>';
  } catch {
    console.error(err);
  }
}
