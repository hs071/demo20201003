/**
 * @name runCodeAnalisys.js
 */
import { gulp, shell, path } from '../common/utils';

/**
 * @name runCodeAnalisys
 * @description exec Code Analisys for Pmd
 */
export default async function runCodeAnalisys() {
  try {
    // target file
    const logFilePath_ = path.relative(__dirname, '.logs'); // LWC（フォルダ）で指定したい
    // file path create
    const logFilePath = path.join(__dirname, logFilePath_);
    console.log('.log Dir Path : ' + logFilePath);
    const fs = require('fs');
    // log folder check
    if (fs.existsSync(logFilePath)) {
      // log folder delete
      const files = fs.readdirSync(logFilePath);
      for (const file in files) {
        fs.unlinkSync(logFilePath + '/' + files[file]);
      }
    }

    // target file
    const reportFilePath_ = path.relative(__dirname, '.report'); // LWC（フォルダ）で指定したい
    // file path create
    const reportFilePath = path.join(__dirname, reportFilePath_);
    console.log('.report Dir Path : ' + '/' + logFilePath);
    // report folder check
    const files = fs.readdirSync(reportFilePath);
    for (const file in files) {
      fs.unlinkSync(reportFilePath + files[file]);
    }

    const commandCodeAnalisys = 'yarn gulp:exec:codeAnalisys';
    const commandCodeQualityCmd = 'yarn gulp:scan:codeQuality';
    gulp
      .src('.')
      .pipe(
        shell(commandCodeAnalisys, {
          verbose: true,
          ignoreErrors: false
        })
      )
      .pipe(
        shell(commandCodeQualityCmd, {
          verbose: true,
          ignoreErrors: false
        })
      );
  } catch (err) {
    console.error(err);
  }
  return gulp.src('.');
}
