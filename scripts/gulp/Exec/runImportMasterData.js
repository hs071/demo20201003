/**
 * @name runImportMasterData.js
 */
import { gulp, shell, argv } from '../common/utils';

/**
 * @name runImportMasterData
 * @description runImportMasterData for Sample
 */
export default async function runImportMasterData(done) {
  try {
    if (argv.ExecMode === undefined) {
      return done(
        new Error(`
         ERROR runImportMasterData 引数が不正です。下記コマンド形式で実行してください
         gulp:exec:ImportMasterData --ExecMode 0(:initialize) or 1(:ImportSampleMaster)`)
      );
    }
    const commandImportMaster = `yarn gulp:master:importMaster --ExecMode ${argv.ExecMode}`;
    gulp.src('.').pipe(
      shell(commandImportMaster, {
        verbose: true,
        ignoreErrors: false
      })
    );
  } catch (err) {
    console.error(err);
  }
  return gulp.src('.');
}
