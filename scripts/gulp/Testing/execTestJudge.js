/**
 * @name execTestJudge.js
 */
import { gulp, shell, path } from '../common/utils';

/**
 * @name execTestJudge
 * @description exec Unit Test for Pmd
 */
export default async function execTestJudge() {
  // 定義
  const minimist = require('minimist');
  // parameter
  const options = minimist(process.argv.slice(2), {
    string: 'user',
    default: {
      user: 'develop' // default
    }
  });
  const options2 = minimist(process.argv.slice(2), {
    string: 'runId',
    default: {
      runId: 'develop' // default
    }
  });
  const username = options.user;
  const testRunId = options2.runId;
  try {
    // Command Create
    const commandUnitTestReport =
      'sfdx force:apex:test:report' +
      ' --targetusername ' +
      username.toString() +
      ' --testrunid ' +
      testRunId.toString() +
      ' --codecoverage --outputdir .reports --resultformat human --verbose || true .reports/test-result-local.txt';

    console.log('test run is normal');
    return gulp.src('.').pipe(
      shell(commandUnitTestReport, {
        verbose: true,
        ignoreErrors: false
      })
    );
  } catch (err) {
    console.error(err);
  }
  return gulp.src('.');
}
