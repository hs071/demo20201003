/**
 * @name execSalesforceEnvTest.js
 */
import { gulp, shell, path } from '../common/utils';
import fs from 'fs';

/**
 * @name execSalesforceEnvTest
 * @description exec Unit Test for Pmd
 */
export default async function execSalesforceEnvTest() {
  // 定義
  const minimist = require('minimist');
  // parameter
  const options = minimist(process.argv.slice(2), {
    string: 'user',
    default: {
      user: 'develop' // default
    }
  });
  const username = options.user;
  try {
    // Command Create
    const commandUnitTest =
      'sfdx force:apex:test:run' +
      ' --targetusername ' +
      username.toString() +
      ' --codecoverage --outputdir .logs --testlevel RunLocalTests --synchronous --resultformat json --json --verbose | jq -r ".result.summary" > .logs/test-run-local-summary.json';
    // target file
    const filePath_ = path.relative(__dirname, '**/*.cls');
    // file path create
    const filePath = path.join(__dirname, filePath_);
    // result file
    const RESULT_FILE = '.logs/test-run-local-summary.json';
    const glob = require('glob');
    console.log('Exec Testing Dir Path : ' + filePath);
    glob(filePath, function (err, files) {
      if (files.length > 0) {
        console.log('Exec Testing Target files : ' + files.length);
        return gulp.src('.').pipe(
          shell(commandUnitTest, {
            verbose: true,
            ignoreErrors: false
          })
        );
      } else {
        console.log('warn:non-files - TestingClass');
        fs.writeFileSync(RESULT_FILE, createDummyFile());
      }
    });
  } catch (err) {
    console.error(err);
  }
  return gulp.src('.');
}

/**
 * @name createDummyFile
 * @description non-exec create dummy file
 */
function createDummyFile() {
  try {
    return '{\n  "status": 0,\n  "outcome": "Dummy",\n  "testRunId": "7074C00000988ax",\n  "testRunCoverage": "100%",\n  "orgWideCoverage": "100%"\n}\n';
  } catch {
    console.error(err);
  }
}
