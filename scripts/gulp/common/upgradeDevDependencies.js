/**
 * @name upgradeDevDependencies.js
 */
import { gulp, shell } from './utils';

/**
 * @name upgradeDevDependencies
 * @description upgrade devDependencies
 */
export default function upgradeDevDependencies() {
  const commandCmd =
    'yarn add --dev --exact ' +
    '@babel/cli ' +
    '@babel/core ' +
    '@babel/preset-env ' +
    '@babel/register ' +
    '@prettier/plugin-xml ' +
    '@salesforce/eslint-config-lwc ' +
    'eslint ' +
    'gulp ' +
    'gulp-babel ' +
    'gulp-shell ' +
    'jsonpath ' +
    'prettier ' +
    'prettier-plugin-apex ' +
    'xml2js';

  return gulp.src('.').pipe(
    shell(commandCmd, {
      verbose: true,
      ignoreErrors: false
    })
  );
}
