/**
 * @name Utils.js
 */
import gulp from 'gulp';
import shell from 'gulp-shell';
import path from 'path';
import fs from 'fs';
import minimist from 'minimist';
import jsonpath from 'jsonpath';
import xml2js from 'xml2js';

const readFileSync = fs.readFileSync;
const writeFileSync = fs.writeFileSync;
const parseStringPromise = new xml2js.Parser().parseStringPromise;
const jsonParse = (data) => JSON.parse(JSON.stringify(data));

export {
  gulp,
  jsonParse,
  jsonpath,
  path,
  parseStringPromise,
  readFileSync,
  shell,
  writeFileSync
};

const ARGV_SLICE_START = 2;

/**
 * @name argv
 * @description コマンドライン引数
 */
export const argv = minimist(process.argv.slice(ARGV_SLICE_START));

/**
 * @name getFiles
 * @description ターゲットPath以下にあるファイル群を取得する
 */
export function getFiles(dir, ignoreFile) {
  let dirAndFiles = fs.readdirSync(dir).filter((file) => {
    return (
      ignoreFile === undefined ||
      ignoreFile === null ||
      ignoreFile === '' ||
      ignoreFile.indexOf(file) === -1
    );
  });

  let files = dirAndFiles.map((item) => {
    const p = path.join(dir, item);
    return fs.statSync(p).isDirectory() ? getFiles(p) : p;
  });

  return Array.prototype.concat(...files);
}
