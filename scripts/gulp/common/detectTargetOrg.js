/**
 * 実行時に必要なモジュールの読み込み
 * @name detectTargetOrg.js
 */
import { gulp, shell } from './utils';

/**
 * @name detectTargetOrg
 * @description 秘密鍵の複合化、組織へログインを実施します。
 */
export default function detectTargetOrg() {
  try {
    // 秘密鍵を復号化
    gulp.src('.').pipe(
      shell(
        `openssl enc -aes-256-cbc -salt -d -pbkdf2` +
          ` -md sha256` +
          ` -in assets/server.key.enc` +
          ` -out assets/server.key` +
          ` -k SomePassword`,
        {
          verbose: true,
          ignoreErrors: false
        }
      )
    );

    // Salesforceのソース組織へログイン
    gulp.src('.').pipe(
      shell(
        `sfdx force:auth:jwt:grant` +
          ` --instanceurl https://test.salesforce.com` +
          ` --clientid 3MVG9pcaEGrGRoTJaOuuPP9SBbBNu_FZd2jnWfSH3GDn3DVxDdzkV9f4NHioAIeMm3COwyhzsDjzDktIsnl0I` +
          ` --jwtkeyfile assets/server.key` +
          ` --username gitlab.user@accenture.com.devops01` +
          ` --setalias ut01`,
        {
          verbose: true,
          ignoreErrors: false
        }
      )
    );
  } catch (err) {
    console.error(err);
  }
  return gulp.src('.');
}
