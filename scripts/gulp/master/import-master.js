/**
 * 実行時に必要なモジュールの読み込み
 * @name import-master.js
 */
import {
  gulp,
  path,
  readFileSync,
  writeFileSync,
  getFiles,
  shell,
  argv
} from '../common/utils';
import mkdirp from 'mkdirp';

/**
 * @name importMasterJson
 * @description マスターをインポートするタスクを実行します。
 */
export default function importMasterJson(done) {
  // 絶対パスを取得
  const filePath_ = path.relative(
    __dirname,
    'scripts/gulp/master/script/delete_all_master_data.apex'
  );
  const filePath = path.join(__dirname, filePath_);
  const wrapRecordsCountExcess = 200;
  const wrapRecordsCountLess = 199;
  const importSampleMaster = 1;

  // コマンドライン引数チェック
  try {
    console.log(argv.ExecMode);
    if (argv.ExecMode === undefined) {
      return done(
        new Error(`
         ERROR importMasterJson 引数が不正です。下記コマンド形式で実行してください
         gulp:Master:importMaster --ExecMode 0:initialize  or 1:ImportSampleMaster`)
      );
    }

    // 環境内の全マスターデータを削除します
    gulp.src('.').pipe(
      shell(`sfdx force:apex:execute --apexcodefile ${filePath} -u ut01`, {
        verbose: true,
        ignoreErrors: false
      })
    );

    // マスターデータが入っているファイルを全量取得
    let masterFileRootPath = path.join(__dirname, 'data');

    if (argv.ExecMode === importSampleMaster) {
      // upsert対象を除外する
      let ignoreFiles = [
        // 'CheckItemMaster__c.json'
      ];

      let masterFiles = getFiles(masterFileRootPath, ignoreFiles);
      for (let masterFilePath of masterFiles) {
        console.log('masterFilePath=' + masterFilePath);
        let masterJson = JSON.parse(readFileSync(masterFilePath));
        if (masterJson.records.length > wrapRecordsCountExcess) {
          // レコード数が200行より大きければ200行ずつに分解する
          let baseFileName = path.basename(
            masterFilePath,
            path.extname(masterFilePath)
          );

          for (
            let r = 0;
            r < masterJson.records.length;
            r = r + wrapRecordsCountLess
          ) {
            let split200Json = {
              records: masterJson.records.slice(r, r + wrapRecordsCountLess)
            };

            let split200JsonPath = path.join(
              '.build-temp',
              'split',
              `${baseFileName}${r + 1}-${r + wrapRecordsCountLess}.json`
            );

            // eslint-disable-next-line consistent-return
            mkdirp(path.dirname(split200JsonPath), function (err) {
              if (err) {
                return done(new Error(err));
              }
              writeFileSync(split200JsonPath, JSON.stringify(split200Json));
            });

            // `sfdx force:data:tree:import --sobjecttreefiles ${split200JsonPath} -u ${argv.orgAlias}`,
            gulp.src('.').pipe(
              shell(
                `sfdx force:data:tree:import --sobjecttreefiles ${split200JsonPath} -u ut01`,
                {
                  verbose: true,
                  ignoreErrors: false
                }
              )
            );
          }
        } else {
          console.log(masterFilePath);

          gulp.src('.').pipe(
            shell(
              `sfdx force:data:tree:import --sobjecttreefiles ${masterFilePath} -u ut01`,
              {
                verbose: true,
                ignoreErrors: false
              }
            )
          );
        }
      }
    }
  } catch (err) {
    console.error(err);
  }
  return gulp.src('.');
}
