/* eslint-disable no-undef */
import {series} from 'gulp';

import upgradeDevDependencies from './scripts/gulp/common/upgradeDevDependencies';
import scanCodeQualityESLint from './scripts/gulp/code-analysis/scanCodeQualityESLint';
import scanCodeQualityPmd from './scripts/gulp/code-analysis/scanCodeQualityPmd';
import scanCodeQualityPrettier from './scripts/gulp/code-analysis/scanCodeQualityPrettier';
import execCodeAnalisysESLint from './scripts/gulp/code-analysis/execCodeAnalisysESLint';
import execCodeAnalisysPmd from './scripts/gulp/code-analysis/execCodeAnalisysPmd';
import execCodeFormatPrettier from './scripts/gulp/code-analysis/execCodeFormatPrettier';
import execRunAnalisys from './scripts/gulp/Exec/runCodeAnalisys';
import execSalesforceEnvTest from './scripts/gulp/Testing/execSalesforceEnvTest';
import execTestJudge from './scripts/gulp/Testing/execTestJudge';
/** - Master Update Demonstration - */
import detectTargetOrg from './scripts/gulp/common/detectTargetOrg';
import importMasterJson from './scripts/gulp/master/import-master';
import runImportMasterData from './scripts/gulp/Exec/runImportMasterData';
/** - Master Update Demonstration - */

exports.upgradeDevDependencies = series(upgradeDevDependencies);
exports.scanCodeQuality = series(scanCodeQualityPrettier, scanCodeQualityESLint, scanCodeQualityPmd);
exports.execCodeAnalisys = series(
  execCodeAnalisysESLint,
  execCodeAnalisysPmd,
  execCodeFormatPrettier
);
exports.runAnalisys = series(execRunAnalisys);
exports.execSFDCEnvTest = series(execSalesforceEnvTest);
exports.execTestJudge = series(execTestJudge);


/** - Master Update Demonstration - */
exports.detectTargetOrg = series(detectTargetOrg);
exports.importMaster = importMasterJson;
exports.runImportMasterData = runImportMasterData;
/** - Master Update Demonstration - */